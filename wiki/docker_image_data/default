##
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# http://wiki.nginx.org/Pitfalls
# http://wiki.nginx.org/QuickStart
# http://wiki.nginx.org/Configuration
#
# Generally, you will want to move this file somewhere, and start with a clean
# file but keep this around for reference. Or just disable in sites-enabled.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

# Default server configuration
server {
        listen 80;
        listen [::]:80;

        server_name mediawiki;

        root /var/www/html;
        index index.php;

        # For correct file uploads
        client_max_body_size    128m; # Equal or more than upload_max_filesize in /etc/php/php.ini
        client_body_timeout     60;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                #try_files $uri $uri/ =404;

                # Avoid index of directories
                autoindex  off;

                try_files $uri $uri/ @mediawiki;
        }
        # pass the PHP scripts to PHP5-fpm server
        #
        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                # With php5-fpm:
                fastcgi_pass unix:/var/run/php5-fpm.sock;
        }
        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        location ~ /\.ht {
                deny all;
        }
        location @mediawiki {
                rewrite ^/(.*)$ /index.php?title=$1&$args;
        }
        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
                try_files $uri /index.php;
                expires max;
                log_not_found off;
        }
        # Restrictions based on the .htaccess files
        # Insert 'mw-config' after installation
        location ^~ /(bin|docs|extensions|includes|maintenance|mw-config|resources|serialized|tests)/ {
                internal;
        }
        location ^~ /images/ {
                try_files $uri /index.php;
        }
        location ~ /\. {
                access_log off;
                log_not_found off; 
                deny all;
        }
        location ^~ /mw-config {
                return 403;
        }
        location = /_.gif {
                expires max;
                empty_gif;
        }
        location ^~ /cache/ {
                deny all;
        }
        location /dumps {
                root /var/www/mediawiki/local;
                autoindex on;
        }
}
